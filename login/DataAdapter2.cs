﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using System.Text;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.IO;
using System.Data;
using static login.VistaLista;

namespace login
{
    public class DataAdapter2 : BaseAdapter <DataTableItem>
    {
        List<DataTableItem> items;

        Activity context;

        public DataAdapter2(Activity context, List<DataTableItem> items) : base()
        {
            this.context = context;
            this.items = items;
        }




        public override long GetItemId(int position)
        {
            return position;
        }

        public override DataTableItem this[int position]
        {
            get { return items[position]; }

        }
        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var itemm = items[position];
            var view = convertView;
            view = context.LayoutInflater.Inflate(Resource.Layout.DataRow, null);
            view.FindViewById<TextView>(Resource.Id.txt_sub).Text = itemm.email;

            var txt_head = view.FindViewById<TextView>(Resource.Id.txt_head);
            txt_head.Text = itemm.nombre;

            var path = System.IO.Path.Combine(System.Environment.GetFolderPath(
                System.Environment.SpecialFolder.Personal), itemm.imagef);


            var backpath = System.IO.Path.Combine(System.Environment.GetFolderPath(
                System.Environment.SpecialFolder.Personal), itemm.backimage);

            var Image = BitmapFactory.DecodeFile(path);

            var Imageback = BitmapFactory.DecodeFile(backpath);

            Image = ResizeBitMap(Image, 100, 100);

            view.FindViewById<ImageView>(Resource.Id.imagef).SetImageDrawable(getRoundedCornerImage(Image, 5));
            view.FindViewById<ImageView>(Resource.Id.back_image).SetImageDrawable(getRoundedCornerImage(Imageback, 2));
            return view;


        }
        public static Android.Support.V4.Graphics.Drawable.RoundedBitmapDrawable
            getRoundedCornerImage(Bitmap image, int cornerRadius)
        {

            var corner = Android.Support.V4.Graphics.Drawable.RoundedBitmapDrawableFactory.Create(null, image);
            corner.CornerRadius = cornerRadius;
            return corner;

        }
        private Bitmap ResizeBitMap(Bitmap originalImage, int widthimageoriginal, int heightimageoriginal)
        {
            Bitmap resizedImage = Bitmap.CreateBitmap(widthimageoriginal, heightimageoriginal, Bitmap.Config.Argb8888);
            float Width = originalImage.Width;
            float Heigth = originalImage.Height;

            var canvas = new Canvas(resizedImage);
            var scale = widthimageoriginal / Width;
            var XTransaltion = 0.0f;
            var YTransaltion = (heightimageoriginal-Heigth*scale)/2.0f;
            var transformation = new Matrix();
            transformation.PostTranslate(XTransaltion, YTransaltion);
            transformation.PreScale(scale, scale);
            var paint = new Paint();
            paint.FilterBitmap = true;
            canvas.DrawBitmap(originalImage, transformation, paint);
            return resizedImage;
        }


    }
}
