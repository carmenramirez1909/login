﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using Android.Content;
using Android.Service.Autofill;
using System.Data;
using System.Security.Cryptography;
using System.Text;


namespace login
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Usuario, contraseña;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            var btningresa = FindViewById<Button>(Resource.Id.btnIngresar);
            var txtusuario = FindViewById<EditText>(Resource.Id.txtUser);
            var txtpassword = FindViewById<EditText>(Resource.Id.txtPassword);
            var Imagen = FindViewById<ImageView>(Resource.Id.imagen);
           
            btningresa.Click += delegate
              {

                  string contraseñacifrada;
                  try
                  {
                      Usuario = txtusuario.Text;
                      contraseña = txtpassword.Text;

                      contraseñacifrada=CifradoSHA(contraseña);
                      
                       var conjunto = new DataSet();
                      var WS = new WebReference.ServicioWeb();
                      conjunto = WS.validarUsuario(Usuario,contraseñacifrada);
                      var dt = new DataTable();
                      dt = conjunto.Tables[0];
                      string variable = "";
                     
                     
                      foreach (DataRow row in dt.Rows)
                      {
                          variable = row["usuario"].ToString();
                      };
                     
                      if (variable !="")
                          
                         
                              cargar();
                         
                          else
                          {
                              Toast.MakeText(this, "Error de contraseña", ToastLength.Short).Show();
                          }


                  }
                  catch (Exception ex)
                  {
                      Toast.MakeText(this, ex.Message, ToastLength.Short).Show();

                  }
              };


        }
        public void cargar()
        {
            Intent Destino = new Intent(this, typeof(Principal2));
            Destino.PutExtra("Usuario", Usuario);
            StartActivity(Destino);
        }

        public string CifradoSHA(string texto)
        {
            using (SHA256 hash = SHA256.Create())
            {
                byte[] arreglo = hash.ComputeHash(Encoding.UTF8.GetBytes(texto));
                StringBuilder cadenasdetexto = new StringBuilder();
                for (int i = 0; i < arreglo.Length; i++)
                {
                    cadenasdetexto.Append(arreglo[i].ToString("X2"));
                }
                return cadenasdetexto.ToString();

            }




        }
    }
}