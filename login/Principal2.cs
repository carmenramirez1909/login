﻿using System;
using System.IO;
using Android.App;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using Android.Content;



namespace login
{
    [Activity(Label = "Principal2")]
    public class Principal2 : Activity
    {
        string Usuario;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Vistaprincipal);
            // Create your application here
            var lblDestino = FindViewById<TextView>(Resource.Id.lblusuario);
            var Imagen = FindViewById<ImageView>(Resource.Id.image);
            var txtfolio = FindViewById<EditText>(Resource.Id.txtfolio);
            var txtdomicilio = FindViewById<EditText>(Resource.Id.txtdomicilio);
            var txtnombre = FindViewById<EditText>(Resource.Id.txtnombre);
            var txtcorreo = FindViewById<EditText>(Resource.Id.txtcorreo);
            var txtedad = FindViewById<EditText>(Resource.Id.txtedad);
            var txtsaldo = FindViewById<EditText>(Resource.Id.txtestado);
            var btnguardar = FindViewById<Button>(Resource.Id.btnguardar);
            var btnbuscar = FindViewById<Button>(Resource.Id.btnbuscar);
            var btnubicacion= FindViewById<ImageView>(Resource.Id.imageView7);
            var btnalta = FindViewById<Button>(Resource.Id.btnvendedor);


            string usuario;
            usuario = Intent.GetStringExtra("Usuario");
            lblDestino.Text = usuario;
            if (usuario == "Carmen")
            {
                ArchivoImagen("https://pbs.twimg.com/profile_images/1216547181496324102/lvpJjQAR_400x400.jpg");
            }
            if (usuario == "jazmin")
            {
                ArchivoImagen("https://scontent.fagu2-1.fna.fbcdn.net/v/t1.0-9/127086972_3283005355160151_4721837352139098638_o.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_ohc=DSqp_3xw64gAX_V-YwG&_nc_ht=scontent.fagu2-1.fna&oh=74f276f15c3f01996220274d6f3349bb&oe=5FF50011");
            }
           

            async void ArchivoImagen(string url)
            {
                var ruta = await DownloadImage(url);
                Android.Net.Uri rutaImagen = Android.Net.Uri.Parse(ruta);
                Imagen.SetImageURI(rutaImagen);
            }
            btnguardar.Click += delegate
            {
                string nombre, domicilio, correo, saldo, cifrar;
                int edad;
               
                try
                {
                    nombre = txtnombre.Text;
                    domicilio = txtdomicilio.Text;
                    correo = txtcorreo.Text;
                    edad = int.Parse(txtedad.Text);

                    cifrar = txtsaldo.Text;
                    saldo= cifradoAES(cifrar, correo, domicilio, edad);
                   
                    var WS = new WebReference.ServicioWeb();
                    if (WS.insertar(nombre, domicilio, correo, edad,saldo))
                        Toast.MakeText(this, "Guardado en mysql", ToastLength.Long).Show();

                    else
                        Toast.MakeText(this, "Favor de consultar a sistemas", ToastLength.Long).Show();

                    txtcorreo.Text = "";
                    txtdomicilio.Text = "";
                    txtcorreo.Text = "";
                    txtedad.Text = "";
                    txtsaldo.Text = "";
                    


                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }
             

            };
            btnubicacion.Click += delegate
            {
                cargar();
            };

            btnalta.Click += delegate
            {
                cargarvend();
            };
            void cargarvend()
            {
                Intent Destino = new Intent(this, typeof(Vendedores));
                //Destino.PutExtra("Usuario", Usuario);
                StartActivity(Destino);
            }
            btnbuscar.Click += delegate
            {
                int ID;
               
                var conjunto = new DataSet();
                DataRow renglon;
                string nombre, domicilio, correo, saldo, cifrar, saldodes;
                int edad;

                try
                {
                    ID = int.Parse(txtfolio.Text);
                    var WS = new WebReference.ServicioWeb();
                    conjunto = WS.Buscar(ID);

                    renglon = conjunto.Tables["Datos"].Rows[0];
                    nombre = renglon["Nombre"].ToString();
                    domicilio= renglon["Domicilio"].ToString();
                    saldo= renglon["Saldo"].ToString();
                    correo = renglon["Correo"].ToString();
                    edad= int.Parse(renglon["Edad"].ToString()); 

                    txtnombre.Text = nombre;
                    txtcorreo.Text = correo;
                    txtdomicilio.Text = domicilio;
                    txtedad.Text = edad.ToString();
                    

                    saldodes = DescifradoAES(saldo, correo, domicilio, edad);
                    txtsaldo.Text = saldodes;


                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }
            };

        }
        public async Task<string> DownloadImage(string url)
        {
            var client = new WebClient();
            byte[] imageData = await client.DownloadDataTaskAsync
                (url);
            var documentspath = System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal);
            var localfilename = "foto1.jpg";
            var localpath = Path.Combine(documentspath, localfilename);
            File.WriteAllBytes(localpath, imageData);
            return localpath;
        }
        public class Datos
        {
            public int folio;
            public string Nombre;
            public string Correo;
            public string Domicilio;
            public int Edad;
            public string Saldo;
        }
        public string cifradoAES(string textocifrar, string password, string vectorinicio, int saltos)
        {
            AesManaged aes = null;
            MemoryStream streamenmemory = null;
            CryptoStream streamcifrado = null;
            try
            {
                var rfc2898 = new Rfc2898DeriveBytes
                    (password, Encoding.UTF8.GetBytes(vectorinicio), saltos);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);
                streamenmemory = new MemoryStream();
                streamcifrado = new CryptoStream(streamenmemory, aes.CreateEncryptor(), CryptoStreamMode.Write);
                byte[] datos = Encoding.UTF8.GetBytes(textocifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();//ejecuta y libera la memoria
                return Convert.ToBase64String(streamenmemory.ToArray()); //saca los datos del cifrado

            }
            catch (System.Exception e)
            {
                return "error";
            }
            finally
            {
                if (streamcifrado != null)
                    streamcifrado.Close();
                if (streamenmemory != null)
                    streamenmemory.Close();
                if (aes != null)
                    aes.Clear();
            }

        }
        public void cargar()
        {
            Intent Destino = new Intent(this, typeof(VistaLista));
            Destino.PutExtra("Usuario", Usuario);
            StartActivity(Destino);
        }

        public string DescifradoAES(string textoadecifrar, string password,
            string vectordeinicio, int saltos)
        {
            AesManaged aes = null;
            MemoryStream streamenmemoria = null;

            try
            {
                var rfc2898 = new Rfc2898DeriveBytes
              (password, Encoding.UTF8.GetBytes(vectordeinicio), saltos);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);

                streamenmemoria = new MemoryStream();
                CryptoStream streamcifrado = new CryptoStream
                    (streamenmemoria, aes.CreateDecryptor(), CryptoStreamMode.Write);

                byte[] datos = Convert.FromBase64String(textoadecifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();

                byte[] arreglodescigrado = streamenmemoria.ToArray();
                if (streamcifrado != null)
                    streamcifrado.Dispose();
                return Encoding.UTF8.GetString(arreglodescigrado, 0,
                 arreglodescigrado.Length);


            }
            catch (System.Exception e)
            {
                return "error";
            }
            finally
            {
                if (streamenmemoria != null)
                    streamenmemoria.Close();

                if (aes != null)
                    aes.Clear();
            }

        }


    }

}
