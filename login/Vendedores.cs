﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Uri = Android.Net.Uri;
using System.IO;
using Path = System.IO.Path;
using Plugin.Geolocator;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Android.Content;
using Android.Graphics;
using Android.Provider;
using System;


namespace login
{
    [Activity(Label = "Activity1")]
    public class Vendedores : Activity
    {
        int camrequestcode = 100;
        Uri FileUri;
        ImageView imagen2;
        EditText txtname, txtaddress, txtage, txtmail, txtrevenue;
        FileStream streamFile;
        double lat, lon;
        Intent intent;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.vendedor);

            // Create your application here

            var btnstorage = FindViewById<Button>(Resource.Id.btnazure);
            txtname = FindViewById<EditText>(Resource.Id.txtname);
            txtaddress = FindViewById<EditText>(Resource.Id.txtaddress);
            txtage = FindViewById<EditText>(Resource.Id.txtage);
            txtmail = FindViewById<EditText>(Resource.Id.txtmail);
            txtrevenue = FindViewById<EditText>(Resource.Id.txtrevenue);
            imagen2 = FindViewById<ImageView>(Resource.Id.imagen2);

            btnstorage.Click += async delegate
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
                    lat = position.Latitude;
                    lon = position.Longitude;

                    var StorageAccount = CloudStorageAccount.Parse
                   ("DefaultEndpointsProtocol=https;AccountName=carmenandroidstorage;AccountKey=itS0EJOn6Vaflwc29zTSc72jf9FTnsLx1c1Sd9rnR4xbacVMp8ZGSOK96sksVEdPO/vQgtugIgRZzfDwcX1sLA==;EndpointSuffix=core.windows.net");
                    var TableNoQL = StorageAccount.CreateCloudTableClient();
                    var Table = TableNoQL.GetTableReference("Administrative");
                    await Table.CreateIfNotExistsAsync();

                    var employee = new Employees("Administrative Staff", txtname.Text);
                    employee.mail = txtmail.Text;
                    employee.revenue = double.Parse(txtrevenue.Text);
                    employee.age = int.Parse(txtage.Text);
                    employee.address = txtaddress.Text;
                    employee.Latitude = lat;
                    employee.Longitude = lon;
                    employee.imagen = txtname.Text + ".jpg";
                    var Store = TableOperation.Insert(employee);
                    await Table.ExecuteAsync(Store);
                    Toast.MakeText(this.ApplicationContext, "Data has been stored", ToastLength.Long).Show();

                    var BlobClient = StorageAccount.CreateCloudBlobClient();
                    var Content = BlobClient.GetContainerReference("images");
                    var resourceBlob = Content.GetBlockBlobReference(txtname.Text + ".jpg");
                    await resourceBlob.UploadFromFileAsync(FileUri.ToString());
                    Toast.MakeText(this.ApplicationContext, "Image stored in Azure Storage Container", ToastLength.Long).Show();
                    txtaddress.Text = "";
                    txtage.Text = "";
                    txtmail.Text = "";
                    txtname.Text = "";
                    txtrevenue.Text = "";
                    

                }
                catch (System.Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();

                }
            };
            imagen2.Click += delegate
            {
                try
                {
                    intent = new Intent(MediaStore.ActionImageCapture);
                    intent.PutExtra(MediaStore.ExtraOutput, FileUri);
                    StartActivityForResult(intent, camrequestcode, bundle);
                }
                catch (System.Exception ex)
                {

                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
            };
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == camrequestcode)
            {
                try
                {
                    if (txtname.Text != null)
                    {
                        FileUri = Android.Net.Uri.Parse(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), txtname.Text + ".jpg"));
                        if (File.Exists(FileUri.ToString()))
                        {
                            Toast.MakeText(this.ApplicationContext, "Image name already exists", ToastLength.Long).Show();
                        }
                        else
                        {
                            streamFile = new FileStream(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                                txtname.Text + ".jpg"), FileMode.Create);
                            var bitmapImage = (Android.Graphics.Bitmap)data.Extras.Get("data");
                            bitmapImage.Compress(Bitmap.CompressFormat.Jpeg, 100, streamFile);
                            streamFile.Close();
                            imagen2.SetImageBitmap(bitmapImage);
                        }
                    }
                }
                catch (System.Exception ex)
                {

                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
            }
        }

    }
    public class Employees : TableEntity
    {
        public Employees(string Category, string Name)
        {
            PartitionKey = Category;
            RowKey = Name;


        }
        public string mail { get; set; }
        public int age { get; set; }
        public string address { get; set; }
        public double revenue { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string imagen { get; set; }

    }
}
    
