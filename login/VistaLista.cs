﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using Android.Content;
using Android.Service.Autofill;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq;
using System.Threading.Tasks;
using Android.Graphics;
using System.IO;
using System.Collections.Generic;


namespace login
{
    [Activity(Label = "VistaLista")]
    public class VistaLista : Activity
    {
        ProgressDialog progress;
        List<employees> Listaempleados = new List<employees>();
        List<DataTableItem> DataTableItems = new List<DataTableItem>();
        ListView listView;
        string itenimage, itenbackimage;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.lista);
            listView = FindViewById<ListView>(Resource.Id.List);
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Cargando... Por favor espere");
            progress.SetCancelable(false);
            progress.Show();
            await LoadDataAzure();
            progress.Hide();

        }
        private async Task LoadDataAzure()
        {
            try
            {
                var StorageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=jazminandroidstorage;AccountKey=m3r5JhclMsolZEakAsxUZXYO+mXJ7hpoVaT0lRqhOK5KJ313O8QB9NQqQPMfkRYWmTSXDRNPHZSSiUONqP3SBA==;EndpointSuffix=core.windows.net");
                var BlObClient = StorageAccount.CreateCloudBlobClient();
                var Container = BlObClient.GetContainerReference("imagenes");
                var TableNoSql = StorageAccount.CreateCloudTableClient();
                var Table = TableNoSql.GetTableReference("employees");
                var Query = new TableQuery<employees>();
                TableContinuationToken  token= null;


                var Data= await Table.ExecuteQuerySegmentedAsync<employees>
                    (Query, token, null, null);
                Listaempleados.AddRange(Data.Results);
                int cMail = 0;
                int cnombre = 0;
                int cimagen = 0;
                int cdireccion = 0;
                int cedad = 0;
                int crevenue = 0;
                int clatitud = 0;
                int clongitud = 0;
                int cbackimage = 0;

                DataTableItems = Listaempleados.Select(r =>
                new DataTableItem()
                {
                    email = Listaempleados.ElementAt(cMail++).email,
                     nombre = Listaempleados.ElementAt(cnombre++).RowKey,
                    imagef = Listaempleados.ElementAt(cimagen++).imagef,
                    direccion = Listaempleados.ElementAt(cdireccion++).direccion,
                    edad = Listaempleados.ElementAt(cedad++).edad,
                    revenue = Listaempleados.ElementAt(crevenue++).revenue,
                    latitud = Listaempleados.ElementAt(clatitud++).revenue,
                    longitud = Listaempleados.ElementAt(clongitud++).longitud,
                    backimage = Listaempleados.ElementAt(cbackimage++).backimage,


                }).ToList();
                int countImages = 0;
                while(countImages<Listaempleados.Count)
                {
                    itenimage = Listaempleados.ElementAt(countImages).imagef;
                    itenbackimage = Listaempleados.ElementAt(countImages).backimage;

                    var BlobFileImage = Container.GetBlockBlobReference(itenimage);
                    var BlobBackFileImage = Container.GetBlockBlobReference(itenbackimage);

                    var pathImage = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    var pathBackImage = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);


                    var fileImageJPG = System.IO.Path.Combine(pathImage, itenimage);
                    var filebackImageJPG = System.IO.Path.Combine(pathImage, itenbackimage);

                    var streamImage = File.OpenWrite(fileImageJPG);
                    var streamBackImage = File.OpenWrite(filebackImageJPG);

                    await BlobFileImage.DownloadToStreamAsync
                        (streamImage);
                    await BlobBackFileImage.DownloadToStreamAsync(streamBackImage);

                    countImages++;
                }
                listView.Adapter = new DataAdapter2(this,DataTableItems);
            }
            catch(System.Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                 
            }

        }
        public class employees : TableEntity
        {
            public employees(string categoria, string nombre)
            {
                PartitionKey = categoria;
                RowKey = nombre;
            }
            public employees()
            { }
            public string direccion { get; set; }
            public int edad { get; set; }
            public string backimage { get; set; }
            public string imagef { get; set; }
            public string email { get; set; }
            public double latitud { get; set; }
            public double longitud { get; set; }
            public double revenue { get; set; }

        }
        public class DataTableItem
        {
            public string nombre { get; set; }
            public string direccion { get; set; }
            public int edad { get; set; }
            public string backimage { get; set; }
            public string imagef { get; set; }
            public string email { get; set; }
            public double latitud { get; set; }
            public double longitud { get; set; }
            public double revenue { get; set; }

        }
    }
}