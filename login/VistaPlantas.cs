﻿using System;
using System.IO;
using Android.App;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.Xml.Serialization;
using System.Data;


namespace login
{
    [Activity(Label = "VistaPlantas")]
    public class VistaPlantas : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.plant);
            // Create your application here
            var lblDestino = FindViewById<TextView>(Resource.Id.lblusuario);
            var Imagen = FindViewById<ImageView>(Resource.Id.imagen);
            var txtfolio = FindViewById<EditText>(Resource.Id.txt_folio);

            var txtnombre = FindViewById<EditText>(Resource.Id.txt_nombre);
            var txtriego = FindViewById<EditText>(Resource.Id.txtriego);
            var txtsiembra = FindViewById<EditText>(Resource.Id.txtsiembra);

            var btnguardar = FindViewById<Button>(Resource.Id.btn_guardar);
            var btnbuscar = FindViewById<Button>(Resource.Id.btn_buscar);

            /* ArchivoImagen("https://www.vippng.com/png/full/158-1585562_abstract-flowers-abstract-watercolor-watercolor-paintings-flores-y.png");


             async void ArchivoImagen(string url)
             {
                 var ruta = await DownloadImage(url);
                 Android.Net.Uri rutaImagen = Android.Net.Uri.Parse(ruta);
                 Imagen.SetImageURI(rutaImagen);
             }
             btnguardar.Click += delegate
             {
                 string Nombre, Riego,Siembra ;


                 try
                 {
                     Nombre = txtnombre.Text;
                     Riego = txtriego.Text;
                     Siembra = txtsiembra.Text;

                     var WS = new WebReference.ServicioWeb();
                     if (WS.insertarplanta(Nombre, Riego, Siembra))
                         Toast.MakeText(this, "Guardado en mysql", ToastLength.Long).Show();

                     else
                         Toast.MakeText(this, "Favor de consultar a sistemas", ToastLength.Long).Show();


                 }
                 catch (Exception ex)
                 {
                     Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                 }
                 txtfolio.Text = "";
                 txtnombre.Text = "";
                 txtriego.Text = "";
                 txtsiembra.Text = "";

             };
             btnbuscar.Click += delegate
             {
                 int ID;
                 var conjunto = new DataSet();
                 DataRow renglon;

                 try
                 {
                     ID = int.Parse(txtfolio.Text);
                     var WS = new WebReference.ServicioWeb();
                     conjunto = WS.BuscarPlanta(ID);
                     renglon = conjunto.Tables["Flores"].Rows[0];

                     txtnombre.Text = renglon["Nombre"].ToString();
                     txtriego.Text = renglon["Riego"].ToString();
                     txtsiembra.Text = renglon["Siembra"].ToString();





                 }
                 catch (Exception ex)
                 {
                     Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                 }

             };

         }
         public async Task<string> DownloadImage(string url)
         {
             var client = new WebClient();
             byte[] imageData = await client.DownloadDataTaskAsync
                 (url);
             var documentspath = System.Environment.GetFolderPath
                 (System.Environment.SpecialFolder.Personal);
             var localfilename = "foto1.jpg";
             var localpath = Path.Combine(documentspath, localfilename);
             File.WriteAllBytes(localpath, imageData);
             return localpath;
         }
         public class Datos
         {
             public int folio;
             public string Nombre;
             public string Riego;
             public string Siembra;

         }*/
        }
    }
}