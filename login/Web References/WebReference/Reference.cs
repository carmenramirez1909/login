﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
// 
#pragma warning disable 1591

namespace login.WebReference {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Data;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ServicioWebSoap", Namespace="http://tempuri.org/")]
    public partial class ServicioWeb : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback VerificarconexionOperationCompleted;
        
        private System.Threading.SendOrPostCallback insertarOperationCompleted;
        
        private System.Threading.SendOrPostCallback validarUsuarioOperationCompleted;
        
        private System.Threading.SendOrPostCallback validarPasswordOperationCompleted;
        
        private System.Threading.SendOrPostCallback BuscarOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ServicioWeb() {
            this.Url = "http://192.168.0.106/ServicioWeb.asmx";
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event VerificarconexionCompletedEventHandler VerificarconexionCompleted;
        
        /// <remarks/>
        public event insertarCompletedEventHandler insertarCompleted;
        
        /// <remarks/>
        public event validarUsuarioCompletedEventHandler validarUsuarioCompleted;
        
        /// <remarks/>
        public event validarPasswordCompletedEventHandler validarPasswordCompleted;
        
        /// <remarks/>
        public event BuscarCompletedEventHandler BuscarCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Verificarconexion", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool Verificarconexion() {
            object[] results = this.Invoke("Verificarconexion", new object[0]);
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void VerificarconexionAsync() {
            this.VerificarconexionAsync(null);
        }
        
        /// <remarks/>
        public void VerificarconexionAsync(object userState) {
            if ((this.VerificarconexionOperationCompleted == null)) {
                this.VerificarconexionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVerificarconexionOperationCompleted);
            }
            this.InvokeAsync("Verificarconexion", new object[0], this.VerificarconexionOperationCompleted, userState);
        }
        
        private void OnVerificarconexionOperationCompleted(object arg) {
            if ((this.VerificarconexionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VerificarconexionCompleted(this, new VerificarconexionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/insertar", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool insertar(string Nombre, string Domiclio, string Correo, int Edad, string Saldo) {
            object[] results = this.Invoke("insertar", new object[] {
                        Nombre,
                        Domiclio,
                        Correo,
                        Edad,
                        Saldo});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void insertarAsync(string Nombre, string Domiclio, string Correo, int Edad, string Saldo) {
            this.insertarAsync(Nombre, Domiclio, Correo, Edad, Saldo, null);
        }
        
        /// <remarks/>
        public void insertarAsync(string Nombre, string Domiclio, string Correo, int Edad, string Saldo, object userState) {
            if ((this.insertarOperationCompleted == null)) {
                this.insertarOperationCompleted = new System.Threading.SendOrPostCallback(this.OninsertarOperationCompleted);
            }
            this.InvokeAsync("insertar", new object[] {
                        Nombre,
                        Domiclio,
                        Correo,
                        Edad,
                        Saldo}, this.insertarOperationCompleted, userState);
        }
        
        private void OninsertarOperationCompleted(object arg) {
            if ((this.insertarCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.insertarCompleted(this, new insertarCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/validarUsuario", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public System.Data.DataSet validarUsuario(string usuario, string password) {
            object[] results = this.Invoke("validarUsuario", new object[] {
                        usuario,
                        password});
            return ((System.Data.DataSet)(results[0]));
        }
        
        /// <remarks/>
        public void validarUsuarioAsync(string usuario, string password) {
            this.validarUsuarioAsync(usuario, password, null);
        }
        
        /// <remarks/>
        public void validarUsuarioAsync(string usuario, string password, object userState) {
            if ((this.validarUsuarioOperationCompleted == null)) {
                this.validarUsuarioOperationCompleted = new System.Threading.SendOrPostCallback(this.OnvalidarUsuarioOperationCompleted);
            }
            this.InvokeAsync("validarUsuario", new object[] {
                        usuario,
                        password}, this.validarUsuarioOperationCompleted, userState);
        }
        
        private void OnvalidarUsuarioOperationCompleted(object arg) {
            if ((this.validarUsuarioCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.validarUsuarioCompleted(this, new validarUsuarioCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/validarPassword", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool validarPassword(string pass) {
            object[] results = this.Invoke("validarPassword", new object[] {
                        pass});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void validarPasswordAsync(string pass) {
            this.validarPasswordAsync(pass, null);
        }
        
        /// <remarks/>
        public void validarPasswordAsync(string pass, object userState) {
            if ((this.validarPasswordOperationCompleted == null)) {
                this.validarPasswordOperationCompleted = new System.Threading.SendOrPostCallback(this.OnvalidarPasswordOperationCompleted);
            }
            this.InvokeAsync("validarPassword", new object[] {
                        pass}, this.validarPasswordOperationCompleted, userState);
        }
        
        private void OnvalidarPasswordOperationCompleted(object arg) {
            if ((this.validarPasswordCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.validarPasswordCompleted(this, new validarPasswordCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Buscar", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public System.Data.DataSet Buscar(int id) {
            object[] results = this.Invoke("Buscar", new object[] {
                        id});
            return ((System.Data.DataSet)(results[0]));
        }
        
        /// <remarks/>
        public void BuscarAsync(int id) {
            this.BuscarAsync(id, null);
        }
        
        /// <remarks/>
        public void BuscarAsync(int id, object userState) {
            if ((this.BuscarOperationCompleted == null)) {
                this.BuscarOperationCompleted = new System.Threading.SendOrPostCallback(this.OnBuscarOperationCompleted);
            }
            this.InvokeAsync("Buscar", new object[] {
                        id}, this.BuscarOperationCompleted, userState);
        }
        
        private void OnBuscarOperationCompleted(object arg) {
            if ((this.BuscarCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.BuscarCompleted(this, new BuscarCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void VerificarconexionCompletedEventHandler(object sender, VerificarconexionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VerificarconexionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VerificarconexionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void insertarCompletedEventHandler(object sender, insertarCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class insertarCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal insertarCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void validarUsuarioCompletedEventHandler(object sender, validarUsuarioCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class validarUsuarioCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal validarUsuarioCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public System.Data.DataSet Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataSet)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void validarPasswordCompletedEventHandler(object sender, validarPasswordCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class validarPasswordCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal validarPasswordCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void BuscarCompletedEventHandler(object sender, BuscarCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class BuscarCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal BuscarCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public System.Data.DataSet Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataSet)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591